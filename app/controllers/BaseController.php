<?php

class BaseController extends Controller {

	public function __construct()
	{
		
		if (Session::has('lang'))
			App::setLocale(Session::get('lang'));
		
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	
	public function changeLanguage($language)
	{
		
		Session::put('lang', $language);
		
		return Redirect::to(Request::header('referer'));
		
	}

}
